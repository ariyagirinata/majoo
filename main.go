package main

import (
	"context"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"majoo/driver"
	"majoo/dto"
	"majoo/manager"
	"majoo/util"
	"net/http"
)

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/list", listUser)
	e.GET("/registerUser", registerUser)
	e.GET("/login", login)
	e.GET("/getUser", getUser)
	e.GET("/updateUser", updateUser)
	driver.InitGorm()
	// Start server
	e.Logger.Fatal(e.Start(":1323"))

}

var mgr = manager.UserManagerImpl{}
// Handler
func listUser(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}

func registerUser(c echo.Context) (err error) {
	u := new(dto.User)
	if err = c.Bind(u); err != nil {
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	validation(c, u)
	err = mgr.CreateUser(context.Background(), *u)
	if err !=nil{
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	return c.JSON(http.StatusOK, dto.Message{Message:"Registrasi Berhasil"})
}

func login(c echo.Context) (err error) {
	u := new(dto.User)
	if err = c.Bind(u); err != nil {
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	err = mgr.Login(context.Background(), *u)
	if err !=nil{
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	return c.JSON(http.StatusOK, dto.Message{Message:"Berhasil Login"})
}

func updateUser(c echo.Context) (err error) {
	u := new(dto.User)
	if err = c.Bind(u); err != nil {
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	validation(c, u)
	resp, err := mgr.UpdateUser(context.Background(), *u)
	if err !=nil{
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	return c.JSON(http.StatusOK, resp)
}

func getUser(c echo.Context) (err error) {
	u := new(dto.User)
	if err = c.Bind(u); err != nil {
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	resp, err := mgr.GetUser(context.Background(), *u)
	if err !=nil{
		return c.JSON(http.StatusInternalServerError, dto.Message{Message:err.Error()})
	}
	return c.JSON(http.StatusOK, resp)
}

func validation (inputContext echo.Context, input *dto.User)(error){
	file, err := inputContext.FormFile("image_name")
	if err != nil {
		return util.UnhandledError{ErrorMessage:"Please upload image_name"}
	}
	input.ImageName = file.Filename
	if input.Username == ""{
		return util.UnhandledError{ErrorMessage:"Please input username"}
	}
	if input.Password == ""{
		return util.UnhandledError{ErrorMessage:"Please input password"}
	}
	if input.NamaLengkap == ""{
		return util.UnhandledError{ErrorMessage:"Please input nama_lengkap"}
	}
	return nil
}