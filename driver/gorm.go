package driver

import (
	"fmt"
	_ "github.com/bmizerany/pq"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"majoo/constant"
)

var dbInstanceGorm = gorm.DB{}

func InitGorm() (*gorm.DB, error){
	db, err := newDBGorm()
	if err != nil {
		return nil, err
	}
	return db,nil
}

func newDBGorm() (*gorm.DB, error) {
	sqlserverInfo := ""
	sqlserverInfo = fmt.Sprintf("sqlserver://%v:%v@%v%v?database=%v",
		constant.USERNAME,constant.PASSWORD,constant.HOST,constant.PORT,constant.DBNAME,
	)

	db, err := gorm.Open(constant.SQL_SERVER_DIALECT, sqlserverInfo)
	if err!=nil{
		return nil, err
	}
	dbInstanceGorm = *db
	if err != nil {
		return nil, err
	}
	fmt.Println(sqlserverInfo)
	return db, nil
}



func GetDBInstanceGorm()(gorm.DB,error){
	if (dbInstanceGorm .DB == nil){
		return dbInstanceGorm,UnhandledError{ErrorMessage:"Unhandled Error"}
	}else{
		return dbInstanceGorm, nil
	}
}

type UnhandledError struct {
	ErrorMessage string
}

func (ce UnhandledError) Error()string{
	return ce.ErrorMessage
}