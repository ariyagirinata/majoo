package repository

import (
	"context"
	"majoo/dto"
)

type UserRepository interface {
	CreateUser(ctx context.Context, user dto.User) (error)
	UpdateUser(ctx context.Context, user dto.User) (*dto.User, error)
	GetUser(ctx context.Context, user dto.User) (*dto.User, error)
	DeleteUser(ctx context.Context, user dto.User) (error)
	ListUser(ctx context.Context) ([]dto.User, error)
	Login(ctx context.Context, user dto.User) (error)
}
