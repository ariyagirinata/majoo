package repository

import (
	"context"
	"fmt"
	"majoo/driver"
	"majoo/dto"
	"majoo/util"
)

type UserRepositoryImpl struct {}


func (impl UserRepositoryImpl)CreateUser(ctx context.Context, param dto.User) ( error){
	dbInstance, err := driver.GetDBInstanceGorm()
	if err!=nil{
		return  err
	}
	query := fmt.Sprintf("INSERT INTO user_login (username, password, nama_lengkap, image_name) VALUES ('%v', '%v', '%v' ,'%v')",
		param.Username, param.Password, param.NamaLengkap, param.ImageName)
	row := dbInstance.Exec(query)
	err = row.Error
	if err != nil {
		return err
	}
	return nil
}

func (impl UserRepositoryImpl)UpdateUser(ctx context.Context, param dto.User) (*dto.User, error){
	dbInstance, err := driver.GetDBInstanceGorm()
	if err!=nil{
		return  nil,err
	}
	query := fmt.Sprintf("update user_login set username = '%v', password='%v', nama_lengkap ='%v', image_name='%v' where id ='%v'",
		param.Username, param.Password, param.NamaLengkap, param.ImageName, param.Id)
	row := dbInstance.Exec(query)
	err = row.Error
	if err != nil {
		return nil, err
	}
	resp, err := impl.GetUser(ctx, param)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (impl UserRepositoryImpl)GetUser(ctx context.Context, param dto.User) (*dto.User, error){
	dbInstance, err := driver.GetDBInstanceGorm()
	if err!=nil{
		return  nil, err
	}
	query := fmt.Sprintf("select id, username, password, nama_lengkap, image_name from user_login where id = '%v' ",
		param.Id)
	row := dbInstance.Raw(query).Row()
	err = row.Scan(&param.Id,&param.Username,&param.Password,&param.NamaLengkap,&param.ImageName)
	if err!=nil{
		return  nil, err
	}
	return &param, nil
}

func (impl UserRepositoryImpl)DeleteUser(ctx context.Context, param dto.User) ( error){
	return nil
}

func (impl UserRepositoryImpl)ListUser(ctx context.Context)  ([]dto.User, error){
	return nil,nil
}

func (impl UserRepositoryImpl)Login(ctx context.Context, param dto.User)  ( error){
	dbInstance, err := driver.GetDBInstanceGorm()
	if err!=nil{
		return  err
	}
	query := fmt.Sprintf("select count(1) from user_login where username = '%v' and password= '%v' ",
		param.Username, param.Password)
	row := dbInstance.Raw(query).Row()
	count := 0
	row.Scan(&count)
	if count>0{
		return nil
	}else{
		return util.UnhandledError{"invalid username or password"}
	}
}