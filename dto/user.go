package dto

type User struct {
	Id int32 `json:"id" form:"id" query:"id"`
	Username string `json:"username" form:"username" query:"username"`
	Password string `json:"password" form:"password" query:"password"`
	NamaLengkap string `json:"nama_lengkap" form:"nama_lengkap" query:"nama_lengkap"`
	ImageName string `json:"image_name" form:"image_name" query:"image_name"`
}