package constant

const (
	HOST  = "localhost"
	PORT  = ":1433"
	USERNAME ="sa"
	PASSWORD ="password"
	DBNAME ="majoo"
	SSL_MODE="disable"
	SQL_SERVER_DIALECT = "mssql"

)
