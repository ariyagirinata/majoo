package model

import (
	"github.com/jinzhu/gorm"
)

func (t User) TableName() string {
	return "user_login"
}

type User struct {
	Id uint `gorm:"not null;AUTO_INCREMENT:TRUE; primary_key"`
	Username string `gorm:"type:varchar(255);not null"`
	Password string `gorm:"type:varchar(255);not null"`
	NamaLengkap string `gorm:"type:varchar(255);not null"`
	ImageName string `gorm:"type:varchar(255);not null"`
}

func(User) Migrate(tx *gorm.DB) error {
	err := tx.AutoMigrate(&User{},).Error

	if err != nil {
		return err
	}
	tx.Model(&User{}).AddUniqueIndex("unique_username", "username")
	return nil
}
