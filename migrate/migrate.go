package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gopkg.in/gormigrate.v1"
	"majoo/driver"
	"majoo/model"
	"time"
)


func main()  {
	db,errInitDB := driver.InitGorm()
	if(errInitDB!=nil){
		fmt.Print(errInitDB.Error())
	}else{
		m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
			{
				ID: time.Now().UTC().String(),
				Migrate: func(tx *gorm.DB) error {

					err := model.User{}.Migrate(tx)
					if err != nil {
						return err
					}
					return nil
				},
			},

		})

		db.LogMode(true)
		err := m.Migrate()
		if err == nil {
			fmt.Println("Migration did run successfully")
		} else {
			fmt.Println("Could not migrate: %v", err)
		}
		defer db.Close()
	}
}