package manager

import (
	"context"
	"majoo/dto"
	"majoo/repository"
)

type UserManagerImpl struct {}

var repo = repository.UserRepositoryImpl{}

func (impl UserManagerImpl)CreateUser(ctx context.Context, param dto.User) (error){
	err := repo.CreateUser(ctx, param)
	if err!=nil{
		return err
	}
	return nil
}

func (impl UserManagerImpl)UpdateUser(ctx context.Context, param dto.User) (*dto.User, error){
	resp, err := repo.UpdateUser(ctx, param)
	if err!=nil{
		return nil, err
	}
	return resp, nil
}

func (impl UserManagerImpl)GetUser(ctx context.Context, param dto.User) (*dto.User, error){
	resp, err := repo.GetUser(ctx, param)
	if err!=nil{
		return nil, err
	}
	return resp, nil
}

func (impl UserManagerImpl)DeleteUser(ctx context.Context, param dto.User) ( error){
	err := repo.DeleteUser(ctx, param)
	if err!=nil{
		return err
	}
	return nil
}

func (impl UserManagerImpl)ListUser(ctx context.Context)  ([]dto.User, error){
	resp, err := repo.ListUser(ctx)
	if err!=nil{
		return nil, err
	}
	return resp, nil
}

func (impl UserManagerImpl)Login(ctx context.Context, param dto.User) ( error){
	err := repo.Login(ctx, param)
	if err!=nil{
		return err
	}
	return nil
}